﻿using MockPractice;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockTests
{
    [TestFixture]
    public class ClientTest
    {
        string mockContent = "mockContent";
        Client client;
        Mock<IService> mockService;
        Mock<IContentFormatter> mockContentFormatter;
        private string mockServiceName;

        [SetUp]
        public void Setup()
        {
            
            mockService = new Mock<IService>();
            mockContentFormatter = new Mock<IContentFormatter>();
            mockService.Setup(l => l.Connect());
            mockService.Setup(l => l.Dispose());
            mockService.Setup(l => l.GetContent(It.IsAny<long>())).Returns(mockContent);
            mockService.Setup(l => l.Name).Returns(mockServiceName);
            
            client = new Client(mockService.Object, mockContentFormatter.Object);
        }

        [Test]
        public void Identity_Should_BeTheClientIdentity()
        {//GetIdentity()
            var result = client.GetIdentity();
            Assert.AreEqual(client.identity.ToString(), result);
        }

        [Test]
        public void ClientIdentity_Should_ContainsFormattedString_After_IdentityIsFormatted()
        {//GetIdentityFormatted()
            var result = client.GetIdentityFormatted();
            StringAssert.StartsWith("<formatted>", result);
            StringAssert.EndsWith("</formatted>", result);
        }

        [Test]
        public void ServiceName_Should_GiveBackTheServiceName()
        {//GetServicename()
            var result = client.GetServiceName();
            mockService.VerifyGet(e => e.Name);
            Assert.AreEqual(mockService.Object.Name, result);
        }

        [Test]
        public void GetContentMethod_Should_GiveBackTheContent()
        {
            var result = client.GetContent(12);

            mockService.Verify(e => e.GetContent(12), Times.Once);
            Assert.AreEqual(mockContent, result);
        }

        [Test]
        public void When_TheServiceIsConnected_Should_NeverCallConnectMethod()
        {
            mockService.Setup(l => l.IsConnected).Returns(true);
            var result = client.GetContent(11);
            mockService.Verify(e => e.Connect(), Times.Never);
        }

        [Test]
        public void When_TheServiceIsNotConnected_Should_CallConnectMethodOnce()
        {
            mockService.Setup(l => l.IsConnected).Returns(false);
            var result = client.GetContent(11);
            mockService.Verify(e => e.Connect(), Times.Once);
        }
    }
}
